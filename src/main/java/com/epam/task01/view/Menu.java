package com.epam.task01.view;

import com.epam.task01.controller.MainController;

import java.util.*;

public class Menu {
    private String name;

    private String text;

    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private MainController mainController;

    private ResourceBundle resourceBundle;

    private Locale locale;

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    private void setMenuActions() {
        Menu menu = new Menu("Main menu", "");

        menu.putAction(resourceBundle.getString("concatParams"), mainController::concatParams);
        menu.putAction(resourceBundle.getString("checkSentence"), mainController::checkSentence);
        menu.putAction(resourceBundle.getString("splitString"), mainController::splitString);
        menu.putAction(resourceBundle.getString("replaceVowels"), mainController::replaceVowels);
        menu.putAction(resourceBundle.getString("sortTextByWordCount"), mainController::sortTextByWordCount);
        menu.putAction(resourceBundle.getString("uniqueWordInFirstSentence"), mainController::findUniqueWordInFirstSentence);
        menu.putAction(resourceBundle.getString("uniqueQuestionsWords"), mainController::questionsUniqueWords);
        menu.putAction(resourceBundle.getString("replaceString"), mainController::replaceString);
        menu.putAction(resourceBundle.getString("alphabeticalSort"), mainController::alphabeticalSort);
        menu.putAction(resourceBundle.getString("sortByVowelsPercentage"), mainController::sortByVowelsPercentage);
        menu.putAction(resourceBundle.getString("sortByFirstConsonantLetter"), mainController::sortByFirstConsonantLetter);
        menu.putAction(resourceBundle.getString("sortByLetterGrowth"), mainController::sortByLetterGrowth);
        menu.putAction(resourceBundle.getString("sortByWordOccurrence"), mainController::sortByWordOccurrence);
        menu.putAction(resourceBundle.getString("removeWordsWithSpecificLength"), mainController::removeWordsWithSpecificLength);
        menu.putAction(resourceBundle.getString("sortByLetterOccurrenceAlphabetical"), mainController::sortByLetterOccurrenceAlphabetical);
        menu.putAction(resourceBundle.getString("removeLetters"), mainController::removeLetters);
        menu.putAction(resourceBundle.getString("replaceWithSubstring"), mainController::replaceWithSubstring);
        menu.putAction(resourceBundle.getString("toUA"), this::internationalizeMenuUkrainian);
        menu.putAction(resourceBundle.getString("toEN"), this::internationalizeMenuEnglish);
        menu.putAction(resourceBundle.getString("toDE"), this::internationalizeMenuGerman);
        menu.putAction(resourceBundle.getString("exit"), () -> System.exit(0));
        activateMenu(menu);
    }

    public Menu(MainController mainController) {
        this.mainController = mainController;
        locale = new Locale("ua", "UK");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenuActions();
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("ua", "UK");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenuActions();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en", "US");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenuActions();
    }

    private void internationalizeMenuGerman() {
        locale = new Locale("de", "DE");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenuActions();
    }

    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
            setMenuActions();
        }
    }

    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }
    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }
}
