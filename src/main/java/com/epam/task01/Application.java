package com.epam.task01;

import com.epam.task01.controller.MainController;
import com.epam.task01.controller.impl.MainControllerImpl;
import com.epam.task01.service.FileService;
import com.epam.task01.service.impl.FileServiceImpl;
import com.epam.task01.view.Menu;

public class Application {
    public static void main(String[] args) {
        FileService fileService = new FileServiceImpl();
        MainController mainController = new MainControllerImpl(fileService);
        new Menu(mainController);
    }
}
