package com.epam.task01.service;

import java.io.IOException;
import java.util.List;

public interface FileService {
    List<String> readFromFile(String path) throws IOException;
}
