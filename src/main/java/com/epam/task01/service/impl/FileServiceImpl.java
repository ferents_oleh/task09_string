package com.epam.task01.service.impl;

import com.epam.task01.service.FileService;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileServiceImpl implements FileService {
    @Override
    public List<String> readFromFile(String filename) throws IOException {
        File file = new File(filename);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        List<String> lines = new ArrayList<>();
        for (String line; (line = bufferedReader.readLine()) != null;) {
            lines.add(line);
        }
        List<String> result = new ArrayList<>();
        for (String line : lines) {
            String[] sentences = line.split("[.!?]");
            result.addAll(Arrays.asList(sentences));
        }
        return result;
    }
}
