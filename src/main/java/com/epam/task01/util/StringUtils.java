package com.epam.task01.util;

import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringUtils {
    public static String concatAllParameters(List<? extends CharSequence> params) {
        StringBuilder result = new StringBuilder();
        for (CharSequence param : params) {
            String p = param.toString().replaceAll("\\s+", "");
            result.append(p);
        }
        return result.toString();
    }

    public static boolean checkSentence(String sentence) {
        Pattern pattern = Pattern.compile("\\b([A-Z][a-z]*)\\b(.$)");
        Matcher matcher = pattern.matcher(sentence);
        return matcher.matches();
    }

    public static String splitString(String text) {
        String[] splitted = text.split("\\bthe|you\\b");
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : splitted) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString().trim();
    }

    public static String replaceVowels(String word) {
        return word.replaceAll("[aeoui]", "_");
    }

    public static List<String> sortTextByWordCount(List<String> text) {
        text.sort(new SentenceComparator());
        return text;
    }

    public static String findUniqueWordInFirstSentence(List<String> text) {
        String firstSentence = text.get(0);
        String[] firstSentenceWords = firstSentence.split(" ");

        String uniqueWord = "";
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String firstSentenceWord : firstSentenceWords) {
                for (String word : words) {
                    if (!word.equals(firstSentenceWord)) {
                        uniqueWord = firstSentenceWord;
                    }
                }
            }
        }
        return uniqueWord;
    }

    public static List<String> questionsUniqueWords(List<String> text, int length) {
        List<String> questions = text
                .stream()
                .filter(s -> s.endsWith("?"))
                .collect(Collectors.toList());
        List<String> uniqueQuestionsWords = questions
                .stream()
                .distinct()
                .collect(Collectors.toList());
        return uniqueQuestionsWords
                .stream()
                .filter(q -> q.length() == length)
                .collect(Collectors.toList());
    }

    public static List<String> replaceWords(List<String> text) {
        for (String sentence : text) {
            String firstWordStartsWithVowel = "";
            String[] words = sentence.split(" ");
            for (String w : words) {
                if (w.startsWith("[аеоуиіАЕОИУІ]")) {
                    firstWordStartsWithVowel = w;
                }
            }

            String longestWord = Arrays.stream(words)
                    .max(Comparator.comparingInt(String::length))
                    .get();

            int firstWordStartsWithVowelIndex = text.indexOf(firstWordStartsWithVowel);
            int longestWordIndex = text.indexOf(longestWord);

            if (firstWordStartsWithVowelIndex > 0 && longestWordIndex > 0) {
                text.add(firstWordStartsWithVowelIndex, longestWord);
                text.add(longestWordIndex, firstWordStartsWithVowel);
            }
        }
        return text;
    }

    public static List<String> sortAlphabetical(List<String> text) {
        List<String> result = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            Collections.sort(words);
            result.addAll(words);
        }
        Collections.sort(result);
        return result;
    }

    public static List<String> sortByVowelsPercentage(List<String> text) {
        List<String> sorted = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            words.sort(new VowelsPercentageComparator());
            sorted.addAll(words);
        }
        sorted.sort(new VowelsPercentageComparator());
        return sorted;
    }

    public static List<String> sortByFirstConsonantLetter(List<String> text) {
        List<String> wordsStartsWithVowel = new ArrayList<>();
        Pattern pattern = Pattern.compile("^[аеоуиі|АЕОИУІ]");
        Matcher matcher;
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String w : words) {
                matcher = pattern.matcher(w);
                if (matcher.lookingAt()) {
                    wordsStartsWithVowel.add(w);
                }
            }
        }
        wordsStartsWithVowel.sort(new FirstConsonantLetterComparator());
        return wordsStartsWithVowel;
    }

    public static List<String> sortByLetterGrowth(List<String> text, char letter) {
        List<String> sorted = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            words.sort(new LetterOccurrenceComparator(letter));
            sorted.addAll(words);
        }
        sorted.sort(new LetterOccurrenceComparator(letter));
        return sorted;
    }

    public static Set<String> sortByWordOccurrence(List<String> text, List<String> wordsToCount) {
        Map<String, Long> wordCount = new HashMap<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            for (String wordToCount : wordsToCount) {
                long count = words.stream().filter(w -> w.equalsIgnoreCase(wordToCount)).count();
                wordCount.put(wordToCount, count);
            }
        }
        wordCount = sortMapByValue(wordCount);
        return wordCount.keySet();
    }

    public static List<String> removeWordWithSpecificLength(List<String> text, int length) {
        List<String> result = new ArrayList<>();
        Predicate<String> predicate = Pattern.compile("^[^аеоуиіАУОЕИІ]").asPredicate();

        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            List<String> filtered = words
                    .stream()
                    .filter(predicate)
                    .filter(w -> w.length() != length)
                    .collect(Collectors.toList());
            result.addAll(filtered);
        }
        return result;
    }

    public static List<String> sortWordsByLetterOccurrence(List<String> text, char letter) {
        List<String> result = sortByLetterGrowth(text, letter);
        result.sort(new LetterOccurrenceAlphabeticComparator(letter));
        return result;
    }

    public static List<String> removeLetters(List<String> text) {
        List<String> result = new ArrayList<>();
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String word : words) {
                char letter = 'a';
                if (word.length() > 0) {
                    letter = word.charAt(0);
                }
                word = word.replaceAll(String.valueOf(letter), "");
                result.add(word);
            }
        }
        return result;
    }

    public static List<String> replaceWithSubstring(List<String> text, int length, String substring) {
        List<String> result = new ArrayList<>();
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String word : words) {
                if (word.length() == length && word.length() != substring.length()) {
                    word = word.replace(substring, "");
                    result.add(word);
                }
            }
        }
        return result;
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortMapByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());
        Collections.reverse(list);
        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    private static int countVowelsPercentage(String word) {
        int vowelCount = 0;
        int vowelsPercentage = 0;
        for (char c : word.toCharArray()) {
            switch (c) {
                case 'а':
                case 'е':
                case 'и':
                case 'і':
                case 'о':
                case 'у':
                    vowelCount++;
                    break;
            }
            if (vowelCount != 0) {
                vowelsPercentage = word.length() / vowelCount;
            }
        }
        return vowelsPercentage;
    }

    static class LetterOccurrenceAlphabeticComparator implements Comparator<String> {
        char letter;

        public LetterOccurrenceAlphabeticComparator(char letter) {
            this.letter = letter;
        }

        @Override
        public int compare(String o1, String o2) {
            long o1LetterCount = o1
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();

            long o2LetterCount = o2
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();
            if (o1LetterCount > o2LetterCount) {
                return -1;
            } else if (o1LetterCount == o2LetterCount) {
                return 0;
            }
            return 1;
        }
    }

    static class VowelsPercentageComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int o1VowelsPercentage = countVowelsPercentage(o1);
            int o2VowelsPercentage = countVowelsPercentage(o2);
            if (o1VowelsPercentage > o2VowelsPercentage) {
                return 1;
            } else if (o1VowelsPercentage == o2VowelsPercentage) {
                return 0;
            }
            return -1;
        }
    }

    static class SentenceComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            String[] word1 = o1.split(" ");
            String[] word2 = o2.split(" ");
            if (word1.length > word2.length) {
                return 1;
            } else if (word1.length == word2.length) {
                return 0;
            }
            return -1;
        }
    }

    static class FirstConsonantLetterComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            o1 = o1.replaceAll("[аеоуиіАЕОИУІ]", "");
            o2 = o2.replaceAll("[аеоуиіАЕОИУІ]", "");
            return o1.compareTo(o2);
        }
    }

    static class LetterOccurrenceComparator implements Comparator<String> {
        char letter;

        public LetterOccurrenceComparator(char letter) {
            this.letter = letter;
        }

        @Override
        public int compare(String o1, String o2) {
            long o1LetterCount = o1
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();

            long o2LetterCount = o2
                    .chars()
                    .filter(ch -> ch == letter)
                    .count();
            if (o1LetterCount > o2LetterCount) {
                return -1;
            } else if (o1LetterCount == o2LetterCount) {
                return 0;
            }
            return 1;
        }
    }
}
