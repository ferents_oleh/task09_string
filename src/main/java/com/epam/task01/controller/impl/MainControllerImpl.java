package com.epam.task01.controller.impl;

import com.epam.task01.controller.MainController;
import com.epam.task01.service.FileService;
import com.epam.task01.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainControllerImpl implements MainController {
    private FileService fileService;

    private String path = "/home/nomorethrow/IdeaProjects/task09_string/src/main/resources/";

    private String filename = "text.txt";

    public MainControllerImpl(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public void concatParams() {
        List<String> params = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String param;
        while(!(param = scanner.nextLine()).isEmpty()) {
            params.add(param);
            System.out.println(StringUtils.concatAllParameters(params));
        }
    }

    @Override
    public void checkSentence() {
        Scanner scanner = new Scanner(System.in);
        String sentence = scanner.nextLine();
        if (StringUtils.checkSentence(sentence)) {
            System.out.println("Sentence is right");
        } else {
            System.out.println("Sentence is wrong");
        }
    }

    @Override
    public void splitString() {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println(StringUtils.splitString(text));
    }

    @Override
    public void replaceVowels() {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        System.out.println(StringUtils.replaceVowels(word));
    }

    @Override
    public void sortTextByWordCount() {
        try {
            List<String> orderedText = StringUtils.sortTextByWordCount(fileService.readFromFile(path + filename));
            orderedText.forEach(s -> System.out.print(s + " "));
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findUniqueWordInFirstSentence() {
        try {
            List<String> text = fileService.readFromFile(path + filename);
            System.out.println(StringUtils.findUniqueWordInFirstSentence(text));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void questionsUniqueWords() {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        try {
            StringUtils.questionsUniqueWords(fileService.readFromFile(path + filename), length)
                    .forEach(w -> System.out.print(w + " "));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replaceString() {
        try {
            StringUtils.replaceWords(fileService.readFromFile(path + filename))
            .forEach(s -> System.out.print(s + " "));
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alphabeticalSort() {
        try {
            StringUtils.sortAlphabetical(fileService.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByVowelsPercentage() {
        try {
            StringUtils.sortByVowelsPercentage(fileService.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByFirstConsonantLetter() {
        try {
            StringUtils.sortByFirstConsonantLetter(fileService.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByLetterGrowth() {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.next().charAt(0);
        try {
            StringUtils.sortByLetterGrowth(fileService.readFromFile(path + filename), letter)
            .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByWordOccurrence() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        List<String> words = Arrays.asList(input.split(" "));
        try {
            StringUtils.sortByWordOccurrence(fileService.readFromFile(path + filename), words)
            .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeWordsWithSpecificLength() {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        try {
            StringUtils.removeWordWithSpecificLength(fileService.readFromFile(path + filename), length)
            .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortByLetterOccurrenceAlphabetical() {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.nextLine().charAt(0);
        try {
            StringUtils.sortWordsByLetterOccurrence(fileService.readFromFile(path + filename), letter)
            .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeLetters() {
        try {
            StringUtils.removeLetters(fileService.readFromFile(path + filename))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replaceWithSubstring() {
        Scanner scanner = new Scanner(System.in);
        int wordLength = scanner.nextInt();
        scanner.nextLine();
        String substring = scanner.nextLine();
        try {
            StringUtils.replaceWithSubstring(fileService.readFromFile(path + filename), wordLength, substring)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
