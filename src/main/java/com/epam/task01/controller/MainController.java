package com.epam.task01.controller;

public interface MainController {
    void concatParams();

    void checkSentence();

    void splitString();

    void replaceVowels();

    void sortTextByWordCount();

    void findUniqueWordInFirstSentence();

    void questionsUniqueWords();

    void replaceString();

    void alphabeticalSort();

    void sortByVowelsPercentage();

    void sortByFirstConsonantLetter();

    void sortByLetterGrowth();

    void sortByWordOccurrence();

    void removeWordsWithSpecificLength();

    void sortByLetterOccurrenceAlphabetical();

    void removeLetters();

    void replaceWithSubstring();
}
